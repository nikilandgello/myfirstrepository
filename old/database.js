const { Sequelize } = require("sequelize");

/** Connecting to a database */
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './User.db'
});

module.exports = sequelize