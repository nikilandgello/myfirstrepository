const { Model, DataTypes } = require(`sequelize`);
const sequelize = require(`./database`);

class UserModel extends Model {}

UserModel.init({
    name: DataTypes.STRING
}, {
    sequelize,
    modelName: 'User',
    tableName: `User`,
    createdAt: false,
    updatedAt: false
});
(async () => {
    await sequelize.sync({});
    console.log(`createTable`);
})();

module.exports = UserModel