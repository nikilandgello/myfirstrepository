const UserModel = require(`./user.model`)

module.exports = class UsersRepository {
    constructor() {};

    async getAll(){
      return await UserModel.findAll()
    };

    async findByName(name){
        return await UserModel.findOne( {where:{name: name}})
    }

    async add(name) {
        return await UserModel.create({name: name});
    };

    async updateByName(name, newName){
        let user = await UserModel.findOne({where:{name:name}});
        user.name = newName;
        return(await user.save());
    };

    async deleteByName(name){
        return(UserModel.destroy({where:{name: name}}));
    };
};

