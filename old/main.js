const express = require(`express`);
const bodyParser = require(`body-parser`);
const { version } = require(`./package.json`);
const sequelize = require(`./database`);

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

sequelize.sync().then(() => console.log(`db is ready`));

const UsersRepository = require (`./users-repository`)
userRepository = new UsersRepository()
app.use(express.json())

app.get(`/users`, async (req, res) => {
    res.send(await userRepository.getAll());
});

app.get(`/users/:name`, async (req, res) => {
    res.send(await userRepository.findByName(req.params.name));
});

app.post(`/users`, async (req, res) => {
    const user = await userRepository.add(req.body.name);
    res.send(user);
});

app.put(`/users/:name`, async (req, res) => {
    res.send(await userRepository.updateByName(req.params.name, req.body.name));
});

app.delete(`/users/:name`, async (req, res) => {
    await userRepository.deleteByName(req.params.name);
    res.sendStatus(200);
});

app.listen(3000, () => {
    console.log(version);
});